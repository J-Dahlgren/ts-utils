import { sleep } from "../src";

describe("Sleep", () => {
    it("Sleeps requested time", async () => {
        const t0 = Date.now();
        await sleep(50);
        expect(Date.now() - t0).toBeGreaterThan(45);
    });
});