import { ReactiveProperty, NullReactiveProperty, ReactiveList, sleep } from "../src";
describe("ReactiveProperty", () => {
    it("Updates Value", () => {
        const rp = new ReactiveProperty<number>(0);
        expect(rp.val).toEqual(0);
        rp.val = 1;
        expect(rp.val).toEqual(1);
    });
    it("Forces notification", async () => {
        const rp = new ReactiveProperty<number>(1);
        const mockFn = jest.fn();
        rp.$.subscribe(() => mockFn());
        rp.forceNotify();
        await sleep(10);
        expect(mockFn).toBeCalledTimes(2);
    });
    it("Emits Values", done => {
        const rp = new ReactiveProperty<number>(1);
        rp.$.subscribe(val => {
            expect(val).toEqual(1);
            done();
        })
    });
    test("NullReactiveProperty inital value works", () => {
        let rp = new NullReactiveProperty<string>();
        expect(rp.val).toBeNull();
        rp.val = "X";
        expect(rp.val).toEqual("X");
        rp = new NullReactiveProperty("X");
        expect(rp.val).toEqual("X");
    })
});
describe("ReactiveList", () => {
    let rp = new ReactiveList<number>();
    let mockFn = jest.fn();
    beforeEach(() => {
        rp = new ReactiveList<number>();
        mockFn = jest.fn();
    });
    it("Notifies when pushed to", async () => {
        rp.$.subscribe(() => mockFn());
        rp.push(1, 2, 3);
        await sleep(10);
        expect(mockFn).toBeCalledTimes(2);
        expect(rp.val).toEqual([1, 2, 3]);
    });
    it("Performs array action", () => {
        rp.$.subscribe(() => mockFn());
        rp.perform(items => items.push(1, 2, 3));
        expect(mockFn).toBeCalledTimes(2);
        expect(rp.val).toEqual([1, 2, 3]);
    });
});