import { BehaviorSubject, Observable } from "rxjs";
export class ReactiveProperty<T = any>{
    protected value: BehaviorSubject<T>;
    constructor(InitialValue: T) {
        this.value = new BehaviorSubject<T>(InitialValue);
    }
    public forceNotify() {
        this.val = this.val;
    }
    public get val(): T {
        return this.value.value;
    }
    public set val(v: T) {
        this.value.next(v);
    }
    public get $(): Observable<T> {
        return this.value.asObservable();
    }
}
export class NullReactiveProperty<T> extends ReactiveProperty<T | null>{
    constructor(InitialValue: (T | null) = null) {
        super(InitialValue);
    }
}

export class ReactiveList<T> extends ReactiveProperty<T[]> {
    constructor(InitialElements: T[] = []) {
        super(InitialElements);
    }
    push(...items: T[]) {
        this.val.push(...items);
        this.forceNotify();
    }
    perform(action: (items:T[]) => void) {
        try {
            action(this.val);
        } catch (error) {
            
        }
        this.forceNotify();
    }

}